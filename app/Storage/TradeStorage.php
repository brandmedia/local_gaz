<?php

namespace App\Storage;


use App\Account;
use App\AccountDetail;
use App\AccountType;
use App\Order;
use App\Partner;
use App\Product;
use App\Stock;
use App\Trade;

class TradeStorage
{
    private $ht = 0;
    private $tva = 0;
    private $ttc = 0;
    private $data = [
        'payments'      => [
            'cash'      => '480',
            'cheque'    => ['price' => '480','operation' => '123456'],
            'transfer'  => ['price' => '480','operation' => '123856']
        ],
        'provider'      => 1,
        'buy'           => [
            'consignees'    => [
                'consign_provider' => [9 => 10]
            ],
        ],
        'sale'          => [
            'gazes'         => [
                'gaze_provider' => [1 => 10]
            ],
            'consignees'    => [
                'consign_provider' => [6 => 10]
            ],
        ],
    ];
    public function trade(array $data)
    {
        // trade
        $invoice = new InvoiceStorage();
        $create = array_merge($invoice->increment(),['partner_id' => $data['partner']],['creator_id' => auth()->id()]);
        return Trade::create($create);
        // sale
            // stock
        // buy
            // stock
        // payment
        // account
    }

    public function sale(array $data)
    {
        // trade
        $trade = $this->trade($data);
        $this->saleProduct($data['sale']['gazes'],$data['partner']);
        $this->saleProduct($data['sale']['consignees'],$data['partner']);
        $trade->update([
            'ht'        => $this->ht,
            'tva'       => $this->tva,
            'ttc'       => $this->ttc
        ]);
    }

    private function saleProduct($data,int $partner_id)
    {

        foreach ($data as $key => $gaze) {
            // create orders
            $order = $this->order($key, $gaze);
            // sub stock gaz
            $stock = Stock::where([
                ['store_id', 1],
                ['product_id', $key]
            ])->first();
            $stock->update(['qt' => $stock->qt - $gaze]);
            // create account tva
            $account = Account::where('account','tva')->first();
            $detail_tva = $account->details()->create([
                'label'     => "Vente Particulier " . $order->product->type->type,
                'detail'    => $order->product->size->size,
                'db'        => $order->tva
            ]);
            $order->account_details()->attach($detail_tva->id);
            // create account store
            $account = Account::where('account','store')->first();
            $account->details()->create([
                'label'     => "Vente Particulier " . $order->product->type->type,
                'detail'    => $order->product->size->size,
                'qt_out'    => $order->qt,
                'db'        => $order->ttc
            ]);
            // create account gain_loss
            $account = Account::where('account','gain_loss')->first();
            $buy = $order->product->prices()->latest()->first();
            $gain = $order->ht - ($order->qt * $buy->buy);
            if($gain > 0) {
                $account->details()->create([
                    'label'     => "Vente Particulier " . $order->product->type->type,
                    'detail'    => $order->product->size->size,
                    'qt_out'    => $order->qt,
                    'db'        => $gain
                ]);
            }
            else {
                $account->details()->create([
                    'label'     => "Vente Particulier  " . $order->product->type->type,
                    'detail'    => $order->product->size->size,
                    'qt_out'    => $order->qt,
                    'cr'        => $gain
                ]);
            }
            // create account Client
            $client = Partner::find($partner_id);
            $account = Account::where('account',$client->name)->first();
            $account->details()->create([
                'label'         => "Achat " . $order->product->type->type,
                'detail'        => $order->product->size->size,
                'qt_enter'      => $order->qt,
                'cr'            => $order->ttc
            ]);
        }
    }

    private function order(int $product_id, int $qt)
    {
        $product = Product::find($product_id);
        $price = $product->prices()->latest()->first();
        $ht = $price->sale * $qt;
        $this->ht = $this->ht + $ht;
        $tva = $ht * 0.1;
        $this->tva = $this->tva + $tva;
        $ttc = $ht + $tva;
        $this->ttc = $this->ttc + $ttc;
        return Order::create([
            'qt'            => $qt,
            'ht'            => $ht,
            'tva'           => $tva,
            'ttc'           => $ttc,
            'product_id'    => $product_id
        ]);
    }


    private function buy(array $data)
    {

    }
}