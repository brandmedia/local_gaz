<?php

use App\Storage\TradeStorage;
use Illuminate\Database\Seeder;

class TradeSeeder extends Seeder
{
    public function run()
    {
        // form
        // gaz
        // consign
        // foreign
        // defective
        $data = [
            'payments'      => [
                'cash'      => '480',
                'cheque'    => ['price' => 'value','operation' => 'value'],
                'transfer'  => ['price' => 'value','operation' => 'value']
            ],
            'partner'      => 1,
            'buy'           => [
                'consignees'    => [9 => 10],
            ],
            'sale'          => [
                'gazes'         => [5 => 10],
                'consignees'    => [6 => 10],
            ],
        ];
        // sale
        $trade= new TradeStorage();
        $trade->sale($data);
        // buy
        // bc
        // bl
    }
}
